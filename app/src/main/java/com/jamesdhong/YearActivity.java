package com.jamesdhong;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.jamesdhong.adapter.YearAdapter;
import com.jamesdhong.base.BaseActivity;
import com.jamesdhong.helper.UtilFile;
import com.jamesdhong.usecase.KarFileUseCase;
import com.jamesdhong.videooke.R;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by FRANCO on 8/6/2016.
 */
public class YearActivity extends  BaseActivity {
    KarFileUseCase karFileUseCase = new KarFileUseCase();
    private ProgressDialog pDialog;
    @BindView(R.id.rv_kar)RecyclerView rv_kar;
    private RecyclerView.LayoutManager layoutManager;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_year);
        ButterKnife.bind(this);
        setupToolbar();
        instantiate();
        pDialog.show();
        karFileUseCase.getList().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Subscriber<HashMap<String,HashMap<String,ArrayList<File>>>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable throwable) {
                pDialog.dismiss();
            }

            @Override
            public void onNext(HashMap<String,HashMap<String,ArrayList<File>>>stringArrayListHashMap) {
                setData(stringArrayListHashMap);
                pDialog.dismiss();
            }
        });
    }

    private void setupToolbar() {
        enableToolBar(R.id.toolbar);
        enableBackMenu();
        setTitle("YEAR");
        setTitleTextColor(Color.WHITE);
    }

    private void instantiate() {
        layoutManager = new LinearLayoutManager(this);
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
    }
    private ArrayList<File> fileList = new ArrayList<>();
    private void getFile(File dir) {
        File listFile[] = UtilFile.getListFileFromFolder(dir);
        for (int i = 0; i < listFile.length; i++) {
            if (listFile[i].isDirectory()) {
                getFile(listFile[i]);
            } else {
                fileList.add(listFile[i]);
            }
        }
    }
    private void setData(HashMap<String,HashMap<String,ArrayList<File>>>stringArrayListHashMap) {
        //set data
        YearAdapter adapter = new YearAdapter(stringArrayListHashMap);
        rv_kar.setAdapter(adapter);
        rv_kar.setLayoutManager(layoutManager);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
