package com.jamesdhong;

import android.media.MediaRecorder;
import android.os.Build;

import java.io.IOException;


public class AudioHelper {
    private MediaRecorder recorder;
    public AudioHelper(){
        if(recorder==null){
            instantiate();
        }
    }

    private void instantiate() {
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        if (Build.VERSION.SDK_INT >= 10) {
            recorder.setAudioSamplingRate(44100);
            recorder.setAudioEncodingBitRate(96000);
            recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
            recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
        } else {
            // older version of Android, use
            // crappy sounding voice codec
            recorder.setAudioSamplingRate(8000);
            recorder.setAudioEncodingBitRate(12200);
            recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        }
    }

    public void record(String path) {
        if(recorder==null){
            instantiate();
        }
        recorder.setOutputFile(path);
        try {
            recorder.prepare();
            recorder.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void stop() {
        if(recorder!=null){
            recorder.stop();
            recorder.reset();
            recorder.release();
        }
        recorder=null;
    }

}
