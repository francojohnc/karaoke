package com.jamesdhong;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.jamesdhong.adapter.AdapterRecord;
import com.jamesdhong.base.BaseActivity;
import com.jamesdhong.helper.UtilFile;
import com.jamesdhong.videooke.R;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityRecord extends BaseActivity {
    @BindView(R.id.rv_kar)RecyclerView rv_kar;
    private RecyclerView.LayoutManager layoutManager;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record);
        ButterKnife.bind(this);
        setupToolbar();
        instantiate();
        String targetFolder = UtilFile.sdCardRoot()+"record"+File.separator;
        File fileFolder = new File(targetFolder);
        getFile(fileFolder);
        setData(fileList);
    }

    private void setupToolbar() {
        enableToolBar(R.id.toolbar);
        enableBackMenu();
        setTitle("MY RECORD FILE");
        setTitleTextColor(Color.WHITE);
    }

    private void instantiate() {
        layoutManager = new LinearLayoutManager(this);
    }
    private ArrayList<File> fileList = new ArrayList<>();
    private void getFile(File dir) {
        File listFile[] = UtilFile.getListFileFromFolder(dir);
        for (int i = 0; i < listFile.length; i++) {
            if (listFile[i].isDirectory()) {
                getFile(listFile[i]);
            } else {
                fileList.add(listFile[i]);
            }
        }
    }
    private void setData(ArrayList<File> fileList) {
        //set data
        AdapterRecord adapter = new AdapterRecord(fileList);
        rv_kar.setAdapter(adapter);
        rv_kar.setLayoutManager(layoutManager);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
