package com.jamesdhong;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;

import com.jamesdhong.base.BaseActivity;
import com.jamesdhong.videooke.R;

import butterknife.ButterKnife;
import butterknife.OnClick;


public class MainActivity extends BaseActivity {
    public static String TAG = MainActivity.class.getSimpleName();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setupToolbar();
    }

    private void setupToolbar() {
        enableToolBar(R.id.toolbar);
        setTitle("KARAOKE");
        setTitleTextColor(Color.WHITE);
    }


    @OnClick({R.id.btn_sing,R.id.btn_record,R.id.btn_instruction,R.id.btn_about})
    void click(View v) {
        Intent i;
        switch (v.getId()){
            case R.id.btn_sing:
                 i = new Intent(this,TabActivity.class);
                 startActivity(i);
                break;
            case R.id.btn_record:
                i = new Intent(this,ActivityRecord.class);
                startActivity(i);
                break;
            case R.id.btn_instruction:
                i = new Intent(this,InstructionActivity.class);
                startActivity(i);
                break;
            case R.id.btn_about:
                i = new Intent(this,AboutActivity.class);
                startActivity(i);
                break;

        }
    }
}
