package com.jamesdhong;

import com.jamesdhong.midi.event.meta.Text;
import java.util.ArrayList;

public class SongLine {
    public float FontSize = 18.0f;
    public long InTime;
    public String SongLine;
    public String SongLineHighlight;
    public ArrayList<Text> WordsList;
    public boolean isTitle = false;
}
