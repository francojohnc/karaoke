package com.jamesdhong;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.jamesdhong.adapter.GenreAdapter;
import com.jamesdhong.base.BaseActivity;
import com.jamesdhong.videooke.R;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by FRANCO on 8/6/2016.
 */
public class GenreActivity extends BaseActivity {
    @BindView(R.id.rv_kar)RecyclerView rv_kar;
    private RecyclerView.LayoutManager layoutManager;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_year);
        ButterKnife.bind(this);
        instantiate();
        setupToolbar();
        getData();
    }
    private void instantiate() {
        layoutManager = new LinearLayoutManager(this);
    }
    private void setData(HashMap<String, ArrayList<File>> fileList) {
        //set data
        GenreAdapter adapter = new GenreAdapter(fileList);
        rv_kar.setAdapter(adapter);
        rv_kar.setLayoutManager(layoutManager);
    }
    private void getData() {
        HashMap<String, ArrayList<File>> files  = (HashMap<String, ArrayList<File>>)getIntent().getExtras().get(Constant.KAR_FILE_MAP_KEY);
        String title = getIntent().getStringExtra(Constant.KAR_FILE_TITLE_KEY);
        if(files!=null&&title!=null){
           setTitle(title);
            setData(files);
        }
    }

    private void setupToolbar() {
        enableToolBar(R.id.toolbar);
        enableBackMenu();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
