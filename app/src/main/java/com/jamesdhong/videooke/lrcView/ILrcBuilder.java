package com.jamesdhong.videooke.lrcView;

import java.util.List;


public interface ILrcBuilder
{
    List<LrcRow> getLrcRows(String rawLrc);
}
