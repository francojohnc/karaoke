package com.jamesdhong.videooke.model;

import java.util.ArrayList;
import java.util.Iterator;

public class Track {
    public String songTitle;
    public ArrayList<TempoTrack> tempoTracks = new ArrayList();
    public ArrayList<TextTrack> textTracks = new ArrayList();
    public String trackCode;
    public int trackLength;
    public String trackName;

    public long getSumTextDelta() {
        long buff = 0;
        Iterator it = this.textTracks.iterator();
        while (it.hasNext()) {
            buff += (long) ((TextTrack) it.next()).timeIn;
        }
        return buff;
    }

    public double getTotalTextTime() {
        double buff = 0.0d;
        Iterator it = this.textTracks.iterator();
        while (it.hasNext()) {
            buff += (double) ((TextTrack) it.next()).timeIn;
        }
        return buff;
    }
}
