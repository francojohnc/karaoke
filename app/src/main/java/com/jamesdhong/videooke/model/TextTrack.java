package com.jamesdhong.videooke.model;

public class TextTrack {
    public long deltaTime;
    public String text;
    public int timeIn;

    public TextTrack(String text, long deltaTime) {
        this.text = text;
        this.deltaTime = deltaTime;
    }
}
