package com.jamesdhong.videooke.model;

public class TempoTrack {
    public int BPM;
    public int MPQN;
    public long deltaTime;
    public int timeIn;

    public TempoTrack(int MPQN, long deltaTime) {
        this.deltaTime = deltaTime;
        this.MPQN = MPQN;
        this.BPM = MPQN == 0 ? 120 : 60000000 / MPQN;
    }
}
