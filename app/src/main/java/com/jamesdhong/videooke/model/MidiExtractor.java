package com.jamesdhong.videooke.model;

import android.util.Log;

import com.jamesdhong.SongLine;
import com.jamesdhong.midi.MidiFile;
import com.jamesdhong.midi.MidiTrack;
import com.jamesdhong.midi.event.MidiEvent;
import com.jamesdhong.midi.event.meta.Lyrics;
import com.jamesdhong.midi.event.meta.Tempo;
import com.jamesdhong.midi.event.meta.Text;
import com.jamesdhong.videooke.BuildConfig;

import java.util.ArrayList;
import java.util.Iterator;

public class MidiExtractor {
    public ArrayList<Tempo> Tempos;
    public ArrayList<Text> Words;

    public MidiExtractor(MidiFile midiFile) {
        int q = midiFile.getResolution();
        this.Words = GetLyrics(midiFile);
        this.Tempos = GetTempos(midiFile);
        Iterator it;
        Text word;
        if (this.Tempos.size() > 1) {
            long LastTempoMS = 0;
            long LastTempoTicks = 0;
            long LastTempoMPQN = 0;
            for (int i = 0; i <= this.Tempos.size(); i++) {
                Tempo curTempo = null;
                long tTime = 0;
                if (i < this.Tempos.size()) {
                    curTempo = (Tempo) this.Tempos.get(i);
                    tTime = LastTempoMS + (((curTempo.getTick() - LastTempoTicks) * LastTempoMPQN) / ((long) q));
                    curTempo.InTime = tTime;
                }
                it = this.Words.iterator();
                while (it.hasNext()) {
                    word = (Text) it.next();
                    if (word.getTick() >= LastTempoTicks && (curTempo == null || word.getTick() < curTempo.getTick())) {
                        word.InTime = LastTempoMS + (((word.getTick() - LastTempoTicks) * LastTempoMPQN) / ((long) q));
                    }
                }
                if (curTempo != null) {
                    LastTempoMS = tTime;
                    LastTempoTicks = curTempo.getTick();
                    LastTempoMPQN = (long) curTempo.getMpqn();
                }
            }
            return;
        }
        int TempoMPQN = Tempo.DEFAULT_MPQN;
        if (this.Tempos.size() == 1) {
            TempoMPQN = ((Tempo) this.Tempos.get(0)).getMpqn();
        }
        it = this.Words.iterator();
        while (it.hasNext()) {
            word = (Text) it.next();
            word.InTime = (word.getTick() * ((long) TempoMPQN)) / ((long) q);
        }
    }

    public static boolean StringIsLineStart(String text) {
        return text.contains("/") || text.contains("\\");
    }

    public static boolean StringIsLineEnd(String text) {
        return text.contains("\r") || text.contains("\n");
    }

    public static String CleanLyricsString(String text) {
        return text.replace("/", BuildConfig.FLAVOR).replace("\\", BuildConfig.FLAVOR).replace("\r", BuildConfig.FLAVOR).replace("\n", BuildConfig.FLAVOR);
    }

    public static ArrayList<SongLine> GetLines(ArrayList<Text> texts, String Title, String Artist) {
        ArrayList<SongLine> songLines = new ArrayList();
        SongLine songLine = null;
        songLines.add(CreateBufferLine(0));
        songLines.add(CreateDetailLine(0, Title));
        songLines.add(CreateDetailLine(0, Artist));
        songLines.add(CreateBufferLine(0));
        Iterator it = texts.iterator();
        while (it.hasNext()) {
            Text event = (Text) it.next();
            Log.d("Song Line", String.format("%d ms : %d : %s", new Object[]{Long.valueOf(event.InTime), Long.valueOf(event.getTick()), event.toString()}));
            String text = event.getText();
            boolean StartFound = StringIsLineStart(text);
            boolean EndFound = StringIsLineEnd(text);
            if (!StartFound && songLine != null) {
                songLine.SongLine += text;
                if (EndFound) {
                    songLine = null;
                }
            } else if (!EndFound) {
                songLine = new SongLine();
                songLine.SongLine = CleanLyricsString(text);
                songLine.InTime = event.InTime;
                songLines.add(songLine);
            }
        }
        long lastInTime = ((Text) texts.get(texts.size() - 1)).InTime;
        songLines.add(CreateBufferLine(1 + lastInTime));
        songLines.add(CreateBufferLine(1 + lastInTime));
        songLines.add(CreateBufferLine(1 + lastInTime));
        return songLines;
    }

    public static SongLine CreateBufferLine(long InTime) {
        SongLine songLine = new SongLine();
        songLine.SongLine = " ";
        songLine.InTime = InTime;
        return songLine;
    }

    public static SongLine CreateDetailLine(long InTime, String text) {
        SongLine songLine = new SongLine();
        songLine.SongLine = text;
        songLine.isTitle = true;
        songLine.InTime = InTime;
        return songLine;
    }

    public static ArrayList<Text> GetLyrics(MidiFile midiFile) {
        ArrayList<Text> lyrics = new ArrayList();
        ArrayList<Text> texts = new ArrayList();
        Iterator it = midiFile.getTracks().iterator();
        while (it.hasNext()) {
            Iterator it2 = ((MidiTrack) it.next()).getEvents().iterator();
            while (it2.hasNext()) {
                Text newText;
                MidiEvent event = (MidiEvent) it2.next();
                if (event.getClass().equals(Lyrics.class)) {
                    newText = new Text(event.getTick(), event.getDelta(), ((Lyrics) event).getLyric());
                    if (!newText.getText().startsWith("@")) {
                        lyrics.add(newText);
                    }
                }
                if (event.getClass().equals(Text.class)) {
                    newText = new Text(event.getTick(), event.getDelta(), ((Text) event).getText());
                    if (!newText.getText().startsWith("@")) {
                        texts.add(newText);
                    }
                }
            }
        }
        return lyrics.size() > 0 ? lyrics : texts;
    }

    public static ArrayList<Tempo> GetTempos(MidiFile midiFile) {
        ArrayList<Tempo> tempos = new ArrayList();
        Iterator it = midiFile.getTracks().iterator();
        while (it.hasNext()) {
            Iterator it2 = ((MidiTrack) it.next()).getEvents().iterator();
            while (it2.hasNext()) {
                MidiEvent event = (MidiEvent) it2.next();
                if (event.getClass().equals(Tempo.class)) {
                    tempos.add((Tempo) event);
                }
            }
        }
        return tempos;
    }
}
