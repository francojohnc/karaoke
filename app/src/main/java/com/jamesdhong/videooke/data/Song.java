package com.jamesdhong.videooke.data;

public class Song {
    public String Artist;
    public int Downloaded;
    public int Favorite;
    public String Filename;
    public int Id;
    public int IdServer;
    public String Link;
    public String Title;
}
