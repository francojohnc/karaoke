package com.jamesdhong.videooke.data;

import android.net.Uri;
import android.os.Environment;

import com.jamesdhong.videooke.BuildConfig;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;

public class SongFileUtil {
    private static final String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()/~'%";

    public static boolean SongFileExists(Song song) throws URISyntaxException {
        return new File(GetSongPath(song)).exists();
    }

    public static String GetSongPath(Song song) throws URISyntaxException {
        String songPath = new File(Environment.getExternalStorageDirectory() + "/videokeking").getAbsolutePath() + new URI(GetEncodedSongLink(song.Link)).getPath().replace("/songs", BuildConfig.FLAVOR).replace("[", BuildConfig.FLAVOR).replace("]", BuildConfig.FLAVOR).replace("`", BuildConfig.FLAVOR).replace("'", BuildConfig.FLAVOR);
        File songDir = new File(songPath).getAbsoluteFile().getParentFile();
        if (!songDir.exists()) {
            songDir.mkdirs();
        }
        return songPath;
    }

    public static String GetEncodedSongLink(String link) {
        return Uri.encode(link, ALLOWED_URI_CHARS);
    }
}
