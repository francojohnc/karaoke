package com.jamesdhong.videooke.data;

import android.provider.BaseColumns;

public final class SongDatabaseContract {
    public static final String[] ALL_COLUMNS = new String[]{"_id", SongTable.COLUMN_NAME_ID_SERVER, SongTable.COLUMN_NAME_TITLE, SongTable.COLUMN_NAME_ARTIST, SongTable.COLUMN_NAME_LINK, SongTable.COLUMN_NAME_FILENAME, SongTable.COLUMN_NAME_FAVORITE, SongTable.COLUMN_NAME_DOWNLOADED};
    public static final String COMMA_SEP = ",";
    public static final String INT_TYPE = " INT";
    public static final String SQL_CREATE_SONG_TABLE = "CREATE TABLE songs (_id INTEGER PRIMARY KEY,idserver INT,title TEXT,artist TEXT,link TEXT,filename TEXT,fav INT,dled INT) ";
    public static final String SQL_DELETE_SONG_TABLE = "DROP TABLE IF EXISTS songs";
    public static final String TEXT_TYPE = " TEXT";

    public static abstract class SongTable implements BaseColumns {
        public static final String COLUMN_NAME_ARTIST = "artist";
        public static final String COLUMN_NAME_DOWNLOADED = "dled";
        public static final String COLUMN_NAME_FAVORITE = "fav";
        public static final String COLUMN_NAME_FILENAME = "filename";
        public static final String COLUMN_NAME_ID_SERVER = "idserver";
        public static final String COLUMN_NAME_LINK = "link";
        public static final String COLUMN_NAME_TITLE = "title";
        public static final String TABLE_NAME = "songs";
    }
}
