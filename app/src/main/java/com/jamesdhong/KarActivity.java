package com.jamesdhong;

import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;

import com.jamesdhong.base.BaseActivity;
import com.jamesdhong.base.BaseFragment;
import com.jamesdhong.fragment.KarFragment;
import com.jamesdhong.videooke.R;

import java.io.File;
import java.util.ArrayList;

import butterknife.ButterKnife;

/**
 * Created by FRANCO on 8/6/2016.
 */
public class KarActivity extends BaseActivity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kar);
        ButterKnife.bind(this);
        setupToolbar();
        getData();


    }
    private void getData() {
        ArrayList<File>files  = (ArrayList<File>)getIntent().getExtras().get(Constant.KAR_FILE_MAP_KEY);
        String title = getIntent().getStringExtra(Constant.KAR_FILE_TITLE_KEY);
        if(files!=null&&title!=null){
            setTitle(title);
            KarFragment karFragment = new KarFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constant.BUNDLE_FRAGMENT_KEY,files);
            karFragment.setArguments(bundle);
            BaseFragment.fragmentReplace(karFragment,getSupportFragmentManager(),R.id.frame_container);
        }
    }
    private void setupToolbar() {
        enableToolBar(R.id.toolbar);
        enableBackMenu();
        setTitleTextColor(Color.BLACK);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
