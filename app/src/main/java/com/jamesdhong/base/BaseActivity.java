package com.jamesdhong.base;

import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.EditText;


public abstract class BaseActivity extends AppCompatActivity {
    public final String TAG = this.getClass().getSimpleName();
    /*toolbar*/
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private Toolbar toolbar;
    protected void enableToolBar(int toolbarId){
        toolbar = (Toolbar)findViewById(toolbarId);
        setSupportActionBar(toolbar);//enable menu
    }
    protected void setTitleTextColor( int color){
        toolbar.setTitleTextColor(color);

    }
    protected void setTitle(String title){
        getSupportActionBar().setTitle(title);
    }
    protected void setNavigationIcon(int icon){
        toolbar.setNavigationIcon(icon);
    }
    protected void hideTitle(){
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }
    protected void enableBackMenu(){
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);//back button
    }
    /*use
     if(checkField()){
                    i = new Intent(this, JoinLifeGroupConfirmActivity.class);
                    startActivity(i);
                }else{
                    showError();
                }
    *  public boolean checkField(){
        if(isEmpty(edt_day))return false;
        if(isEmpty(edt_time))return false;
        if(isEmpty(edt_location))return false;
        return true;
    * */
    protected boolean isEmpty(EditText editText){
        if(editText.getText().toString().isEmpty()) {
            editText.setError("This field is required");
            return true;
        }
        return false;
    }
}

