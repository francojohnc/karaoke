package com.jamesdhong.base;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;


public abstract class BaseFragment extends Fragment {
    public final String TAG = this.getClass().getSimpleName();
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    //BaseFragment.fragmentAddToBackStack(new HomeFragment(),getSupportFragmentManager(),R.id.frame_container);
    public static void fragmentAddToBackStack(Fragment fragment,FragmentManager getSupportFragmentManager,int frame_container){
        getSupportFragmentManager.beginTransaction().replace(frame_container, fragment).addToBackStack(fragment.getClass().getName()).commit();
    }
    public static void fragmentAddToBackStack(Fragment fragment,FragmentManager getSupportFragmentManager,int frame_container,Bundle bundle){
        fragment.setArguments(bundle);
        getSupportFragmentManager.beginTransaction().replace(frame_container, fragment).addToBackStack(fragment.getClass().getName()).commit();
    }
    public static void fragmentReplace(Fragment fragment,FragmentManager getSupportFragmentManager,int frame_container){
        getSupportFragmentManager.beginTransaction().replace(frame_container, fragment).commit();
    }
    public static void fragmentReplace(Fragment fragment,FragmentManager getSupportFragmentManager,int frame_container,Bundle bundle){
        fragment.setArguments(bundle);
        getSupportFragmentManager.beginTransaction().replace(frame_container, fragment).commit();
    }
}