package com.jamesdhong;

import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;

import com.jamesdhong.base.BaseActivity;
import com.jamesdhong.videooke.R;

import butterknife.ButterKnife;

public class InstructionActivity extends BaseActivity{
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instruction);
        ButterKnife.bind(this);
        setupToolbar();

    }

    private void setupToolbar() {
        enableToolBar(R.id.toolbar);
        enableBackMenu();
        setTitle("INSTRUCTION");
        setTitleTextColor(Color.WHITE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
