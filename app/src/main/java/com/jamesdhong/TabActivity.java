package com.jamesdhong;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.jamesdhong.adapter.FragmentTabAdapter;
import com.jamesdhong.fragment.KarFragment;
import com.jamesdhong.fragment.YearFragment;
import com.jamesdhong.usecase.KarFileUseCase;
import com.jamesdhong.videooke.R;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


public class TabActivity extends AppCompatActivity {
    public static final String TAG = TabActivity.class.getSimpleName();
    private TabLayout tabLayout;
    private ProgressDialog pDialog;
    private ViewPager viewPager;
    KarFileUseCase karFileUseCase = new KarFileUseCase();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab);
        instantiate();
        pDialog.show();
        karFileUseCase.getList().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Subscriber<HashMap<String,HashMap<String,ArrayList<File>>>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable throwable) {
                pDialog.dismiss();
            }

            @Override
            public void onNext(HashMap<String,HashMap<String,ArrayList<File>>>stringArrayListHashMap) {
                setupViewPager(stringArrayListHashMap);
                pDialog.dismiss();
            }
        });
    }

    private  ArrayList<File>  getListKar(HashMap<String, HashMap<String, ArrayList<File>>> stringArrayListHashMap) {
        ArrayList<File> listKar = new ArrayList<>();
        for(Map.Entry<String, HashMap<String, ArrayList<File>>> x: stringArrayListHashMap.entrySet()){
            HashMap<String, ArrayList<File>> listHashMap = x.getValue();
            for(Map.Entry<String, ArrayList<File>> z: listHashMap.entrySet()){
                ArrayList<File>list = z.getValue();
                listKar.addAll(list);
            }
        }
        return listKar;
    }


    private void instantiate() {
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
    }
    private void setupViewPager(HashMap<String,HashMap<String,ArrayList<File>>>stringArrayListHashMap) {
        FragmentTabAdapter adapter = new FragmentTabAdapter(getSupportFragmentManager());
        Bundle bundle;
        /*fragment kar*/
        KarFragment karFragment = new KarFragment();
        bundle = new Bundle();
        bundle.putSerializable(Constant.BUNDLE_FRAGMENT_KEY,getListKar(stringArrayListHashMap));
        karFragment.setArguments(bundle);
        /*year fragment*/
        YearFragment yearFragment = new YearFragment();
        bundle = new Bundle();
        bundle.putSerializable(Constant.BUNDLE_FRAGMENT_KEY,stringArrayListHashMap);
        yearFragment.setArguments(bundle);
        /*add fragment*/
        adapter.addFragment(karFragment,"SONG");
        adapter.addFragment(yearFragment,"YEAR");
        /*set adapter*/
        viewPager.setAdapter(adapter);
        adapter.addTabLayout(tabLayout, viewPager);
    }
}
