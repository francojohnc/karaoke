package com.jamesdhong.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.jamesdhong.VideokePlayer;
import com.jamesdhong.videooke.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class SaveRecordDialog extends Dialog {
    @BindView(R.id.btn_save)Button btn_save;
    private View.OnClickListener onClickListener;
    private Context context;
    public SaveRecordDialog(Context context,View.OnClickListener onClickListener) {
        super(context);
        this.context=context;
        this.onClickListener = onClickListener;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_save_record);
        ButterKnife.bind(this);
        btn_save.setOnClickListener(onClickListener);
    }
    @OnClick({R.id.btn_continue,R.id.btn_exit})
    void click(View v) {
        switch (v.getId()){
            case R.id.btn_continue:
                 dismiss();
                break;
            case R.id.btn_exit:
                dismiss();
                ((VideokePlayer)context).finish();
                break;
        }
    }
}
