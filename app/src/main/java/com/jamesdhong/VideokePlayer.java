package com.jamesdhong;

import android.content.Intent;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnErrorListener;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SeekBar;

import com.jamesdhong.dialog.SaveRecordDialog;
import com.jamesdhong.helper.ToolbarCreator;
import com.jamesdhong.helper.UtilFile;
import com.jamesdhong.midi.MidiFile;
import com.jamesdhong.midi.event.meta.Tempo;
import com.jamesdhong.midi.event.meta.Text;
import com.jamesdhong.playheadset.PlayHeadSetActivity;
import com.jamesdhong.videooke.BuildConfig;
import com.jamesdhong.videooke.R;
import com.jamesdhong.videooke.model.MidiExtractor;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VideokePlayer extends AppCompatActivity {
    private  AudioHelper audioHelper;
    private int CurLine;
    private int CurrentPosition;
    private ImageButton btnPlay;
    private ToolbarCreator toolbarCreator;
    File kar;
    boolean isRecord = false;
    @BindView(R.id.seekbar) SeekBar seekbar;
    private Runnable LyricsScroller = new Runnable() {
        public void run() {
            if (!VideokePlayer.this.isStopped) {
                VideokePlayer.this.CurLine = 0;
                while (!VideokePlayer.this.isStopped) {
                    if (VideokePlayer.this.mediaPlayer != null && VideokePlayer.this.mediaPlayer.isPlaying()) {
                        VideokePlayer.this.CurrentPosition = VideokePlayer.this.mediaPlayer.getCurrentPosition();
                    }
                    int CurrentMediaPlayerTime = VideokePlayer.this.CurrentPosition + ((int) (1000.0f));
                    int ctr = 0;
                    while (ctr < VideokePlayer.this.songLineAdapter.songLines.size()) {
                        SongLine songLine = (SongLine) VideokePlayer.this.songLineAdapter.songLines.get(ctr);
                        String CurSongLineWord = BuildConfig.FLAVOR;
                        Iterator it = songLine.WordsList.iterator();
                        while (it.hasNext()) {
                            Text word = (Text) it.next();
                            if (((int) (word.InTime / 1000)) < CurrentMediaPlayerTime) {
                                CurSongLineWord = CurSongLineWord + MidiExtractor.CleanLyricsString(word.getText());
                                if (VideokePlayer.this.CurLine == ctr && songLine.WordsList.indexOf(word) == songLine.WordsList.size() - 1) {
                                    VideokePlayer.this.ScrollToLine(ctr);
                                }
                            }
                        }
                        songLine.SongLineHighlight = CurSongLineWord;
                        if (CurrentMediaPlayerTime >= ((int) songLine.InTime) / 1000 && (ctr == VideokePlayer.this.songLineAdapter.songLines.size() - 1 || CurrentMediaPlayerTime <= ((int) ((SongLine) VideokePlayer.this.songLineAdapter.songLines.get(ctr + 1)).InTime) / 1000)) {
                            VideokePlayer.this.CurLine = ctr;
                        }
                        ctr++;
                    }
                    VideokePlayer.this.handler.post(new Runnable() {
                        public void run() {
                            VideokePlayer.this.songLineAdapter.notifyDataSetChanged();
                        }
                    });
                    seekbar.setProgress((int) (((float) mediaPlayer.getCurrentPosition()/mediaPlayer.getDuration())*100));
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    };
    ArrayList<Tempo> Tempos;
    ArrayList<Text> Words;
    Handler handler = new Handler();
    private boolean isStopped = false;
    private int itemVisibleCount = 0;
    ListView lvSongLines;
    MediaPlayer mediaPlayer;
    SongLineAdapter songLineAdapter;


    protected void onCreate(Bundle savedInstanceState) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_videoke_player);
        ButterKnife.bind(this);
        instantiate();
        getData();
        this.lvSongLines = (ListView) findViewById(R.id.lvSongLines);
        PlaySong();
        Intent i = new Intent(this, PlayHeadSetActivity.class);
        startActivity(i);
    }

    private void getData() {
         kar = (File)getIntent().getExtras().get(Constant.KAR_FILE_KEY);
         isRecord=getIntent().getBooleanExtra(Constant.KAR_RECORD_KEY,false);
    }

    private void instantiate() {
        btnPlay=(ImageButton)findViewById(R.id.btnPlay);
        toolbarCreator = new ToolbarCreator(this,R.id.toolbar);
        toolbarCreator.enableBackMenu();
        audioHelper = new AudioHelper();

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    public void play(View v){
        playPause();
    }

    private void playPause() {
        if(mediaPlayer.isPlaying()){
            if(mediaPlayer!=null){
                mediaPlayer.pause();
                btnPlay.setImageResource(R.drawable.btn_play);
                if(isRecord){
                    showRecordDialog();
                }
            }
        }else{
            if(mediaPlayer!=null){
                mediaPlayer.start();
                btnPlay.setImageResource(R.drawable.btn_pause);
            }
        }
    }

    private void showRecordDialog() {
        SaveRecordDialog saveRecordDialog = new SaveRecordDialog(this, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                audioHelper.stop();
                finish();
            }
        });
        saveRecordDialog.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(isRecord){
            audioHelper.stop();
        }
    }

    public void onPause() {
        super.onPause();
        if(mediaPlayer!=null){
            mediaPlayer.pause();
            btnPlay.setImageResource(R.drawable.btn_play);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(mediaPlayer !=null){
            mediaPlayer.start();
            btnPlay.setImageResource(R.drawable.btn_pause);
        }
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }
    private void LoadSongLine(ArrayList<SongLine> songLines) {
        if (this.songLineAdapter != null) {
            this.songLineAdapter.songLines.clear();
            this.songLineAdapter.clear();
            this.songLineAdapter = null;
        }
        this.songLineAdapter = new SongLineAdapter(this, R.layout.item_song_lines, songLines);
        this.lvSongLines.setAdapter(this.songLineAdapter);
    }

    public void LoadSong(String filePath) {
        IOException e;
        try {
            InputStream fileInputStream = new FileInputStream(filePath);
            try {
                MidiExtractor midiExtractor = new MidiExtractor(new MidiFile(fileInputStream));
                this.Words = midiExtractor.Words;
                this.Tempos = midiExtractor.Tempos;
                ArrayList<SongLine> songLines = MidiExtractor.GetLines(this.Words, "", "");
                int i = 0;
                while (i < songLines.size()) {
                    SongLine songLine = (SongLine) songLines.get(i);
                    songLine.WordsList = new ArrayList();
                    Iterator it = this.Words.iterator();
                    while (it.hasNext()) {
                        Text word = (Text) it.next();
                        if (word.InTime >= songLine.InTime && (i == songLines.size() - 1 || word.InTime < ((SongLine) songLines.get(i + 1)).InTime)) {
                            songLine.WordsList.add(word);
                        }
                    }
                    i++;
                }
                LoadSongLine(songLines);
            } catch (IOException e2) {
                e = e2;
                e.printStackTrace();
            }
        } catch (IOException e3) {
            e = e3;
            e.printStackTrace();
        }
    }

    public void PlaySong() {
        Exception e;
        if (this.mediaPlayer != null) {
            this.mediaPlayer.start();
            return;
        }
        try {
            String targetFolder = UtilFile.sdCardRoot()+"record"+File.separator;
            File fileFolder = new File(targetFolder);
            if(!UtilFile.isExist(fileFolder)){
                fileFolder.mkdir();
            }

            if(isRecord){
                audioHelper.record(targetFolder+kar.getName()+".mp3");
            }
            toolbarCreator.setTitle(kar.getName().replace(".kar",""));
            String songPath = kar.getPath();
            this.mediaPlayer = new MediaPlayer();
            this.mediaPlayer.setDataSource(songPath);
            this.mediaPlayer.prepare();
            LoadSong(songPath);
            this.mediaPlayer.setOnErrorListener(new OnErrorListener() {
                public boolean onError(MediaPlayer mp, int what, int extra) {
                    return false;
                }
            });
            this.mediaPlayer.start();
            new Thread(this.LyricsScroller).start();
        } catch (IOException e2) {
            e = e2;
            e.printStackTrace();
        }
    }

    private void ScrollToLine(final int i) {
        this.handler.post(new Runnable() {
            public void run() {
                int newItemVisibleCount = ((VideokePlayer.this.lvSongLines.getLastVisiblePosition() - VideokePlayer.this.lvSongLines.getFirstVisiblePosition()) / 2) - 1;
                if (VideokePlayer.this.itemVisibleCount < newItemVisibleCount - 1 || VideokePlayer.this.itemVisibleCount > newItemVisibleCount + 1) {
                    VideokePlayer.this.itemVisibleCount = newItemVisibleCount;
                }
                Log.d("visibleItemCount", String.format("%d", new Object[]{Integer.valueOf(VideokePlayer.this.itemVisibleCount)}));
                if (i >= VideokePlayer.this.itemVisibleCount) {
                    VideokePlayer.this.lvSongLines.smoothScrollToPositionFromTop(i - VideokePlayer.this.itemVisibleCount, 0, 500);
                }
            }
        });
    }

}
