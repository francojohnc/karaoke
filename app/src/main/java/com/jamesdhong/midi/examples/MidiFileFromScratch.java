package com.jamesdhong.midi.examples;

import com.jamesdhong.midi.MidiFile;
import com.jamesdhong.midi.MidiTrack;
import com.jamesdhong.midi.event.MidiEvent;
import com.jamesdhong.midi.event.NoteOff;
import com.jamesdhong.midi.event.NoteOn;
import com.jamesdhong.midi.event.meta.Tempo;
import com.jamesdhong.midi.event.meta.TimeSignature;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class MidiFileFromScratch {
    public static void main(String[] args) {
        MidiTrack tempoTrack = new MidiTrack();
        MidiTrack noteTrack = new MidiTrack();
        MidiEvent ts = new TimeSignature();
//        ts.setTimeSignature(4, 4, 24, 8);
        MidiEvent t = new Tempo();
//        t.setBpm(228.0f);
        tempoTrack.insertEvent(ts);
        tempoTrack.insertEvent(t);
        for (int i = 0; i < 80; i++) {
            int pitch = i + 1;
            NoteOn on = new NoteOn((long) (i * MidiFile.DEFAULT_RESOLUTION), 0, pitch, 100);
            NoteOff off = new NoteOff((long) ((i * MidiFile.DEFAULT_RESOLUTION) + 120), 0, pitch, 0);
            noteTrack.insertEvent(on);
            noteTrack.insertEvent(off);
            noteTrack.insertNote(0, pitch + 2, 100, (long) (i * MidiFile.DEFAULT_RESOLUTION), 120);
        }
        ArrayList<MidiTrack> tracks = new ArrayList();
        tracks.add(tempoTrack);
        tracks.add(noteTrack);
        try {
            new MidiFile(MidiFile.DEFAULT_RESOLUTION, tracks).writeToFile(new File("exampleout.mid"));
        } catch (IOException e) {
            System.err.println(e);
        }
    }
}
