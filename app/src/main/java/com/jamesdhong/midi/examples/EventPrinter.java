package com.jamesdhong.midi.examples;

import com.jamesdhong.midi.MidiFile;
import com.jamesdhong.midi.event.MidiEvent;
import com.jamesdhong.midi.event.NoteOn;
import com.jamesdhong.midi.event.meta.Tempo;
import com.jamesdhong.midi.util.MidiEventListener;
import com.jamesdhong.midi.util.MidiProcessor;

import java.io.File;
import java.io.IOException;

public class EventPrinter implements MidiEventListener {
    private String mLabel;

    public EventPrinter(String label) {
        this.mLabel = label;
    }

    public void onStart(boolean fromBeginning) {
        if (fromBeginning) {
            System.out.println(this.mLabel + " Started!");
        } else {
            System.out.println(this.mLabel + " resumed");
        }
    }

    public void onEvent(MidiEvent event, long ms) {
        System.out.println(this.mLabel + " received event: " + event);
    }

    public void onStop(boolean finished) {
        if (finished) {
            System.out.println(this.mLabel + " Finished!");
        } else {
            System.out.println(this.mLabel + " paused");
        }
    }

    public static void main(String[] args) {
        try {
            MidiFile midi = new MidiFile(new File("inputmid.mid"));
            MidiProcessor processor = new MidiProcessor(midi);
            EventPrinter ep = new EventPrinter("Individual Listener");
            processor.registerEventListener(ep, Tempo.class);
            processor.registerEventListener(ep, NoteOn.class);
            processor.registerEventListener(new EventPrinter("Listener For All"), MidiEvent.class);
            processor.start();
            try {
                Thread.sleep(10000);
                processor.stop();
                Thread.sleep(10000);
                processor.start();
            } catch (Exception e) {
            }
            MidiFile midiFile = midi;
        } catch (IOException e2) {
            System.err.println(e2);
        }
    }
}
