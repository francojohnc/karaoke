package com.jamesdhong.midi.util;

import android.support.v4.media.TransportMediator;

import com.jamesdhong.midi.event.meta.MetaEvent;

import java.io.IOException;
import java.io.InputStream;

public class VariableLengthInt {
    private byte[] mBytes;
    private int mSizeInBytes;
    private int mValue;

    public VariableLengthInt(int value) {
        setValue(value);
    }

    public VariableLengthInt(InputStream in) throws IOException {
        parseBytes(in);
    }

    public void setValue(int value) {
        this.mValue = value;
        buildBytes();
    }

    public int getValue() {
        return this.mValue;
    }

    public int getByteCount() {
        return this.mSizeInBytes;
    }

    public byte[] getBytes() {
        return this.mBytes;
    }

    private void parseBytes(InputStream in) throws IOException {
        int i;
        int[] ints = new int[4];
        this.mSizeInBytes = 0;
        this.mValue = 0;
        int shift = 0;
        int b = in.read();
        while (this.mSizeInBytes < 4) {
            boolean variable;
            this.mSizeInBytes++;
            if ((b & TransportMediator.FLAG_KEY_MEDIA_NEXT) > 0) {
                variable = true;
            } else {
                variable = false;
            }
            if (!variable) {
                ints[this.mSizeInBytes - 1] = b & MetaEvent.SEQUENCER_SPECIFIC;
                break;
            } else {
                ints[this.mSizeInBytes - 1] = b & MetaEvent.SEQUENCER_SPECIFIC;
                b = in.read();
            }
        }
        for (i = 1; i < this.mSizeInBytes; i++) {
            shift += 7;
        }
        this.mBytes = new byte[this.mSizeInBytes];
        for (i = 0; i < this.mSizeInBytes; i++) {
            this.mBytes[i] = (byte) ints[i];
            this.mValue += ints[i] << shift;
            shift -= 7;
        }
    }

    private void buildBytes() {
        if (this.mValue == 0) {
            this.mBytes = new byte[1];
            this.mBytes[0] = (byte) 0;
            this.mSizeInBytes = 1;
            return;
        }
        int i;
        this.mSizeInBytes = 0;
        int[] vals = new int[4];
        int tmpVal = this.mValue;
        while (this.mSizeInBytes < 4 && tmpVal > 0) {
            vals[this.mSizeInBytes] = tmpVal & MetaEvent.SEQUENCER_SPECIFIC;
            this.mSizeInBytes++;
            tmpVal >>= 7;
        }
        for (i = 1; i < this.mSizeInBytes; i++) {
            vals[i] = vals[i] | TransportMediator.FLAG_KEY_MEDIA_NEXT;
        }
        this.mBytes = new byte[this.mSizeInBytes];
        for (i = 0; i < this.mSizeInBytes; i++) {
            this.mBytes[i] = (byte) vals[(this.mSizeInBytes - i) - 1];
        }
    }

    public String toString() {
        return MidiUtil.bytesToHex(this.mBytes) + " (" + this.mValue + ")";
    }
}
