package com.jamesdhong.midi.util;

import com.jamesdhong.midi.event.MidiEvent;
import com.jamesdhong.midi.event.meta.TimeSignature;

public class MetronomeTick extends MidiEvent {
    private int mCurrentBeat;
    private int mCurrentMeasure;
    private int mMetronomeFrequency;
    private double mMetronomeProgress;
    private int mResolution;
    private TimeSignature mSignature;

    public MetronomeTick(TimeSignature sig, int resolution) {
        super(0, 0);
        this.mResolution = resolution;
        setTimeSignature(sig);
        this.mCurrentMeasure = 1;
    }

    public void setTimeSignature(TimeSignature sig) {
        this.mSignature = sig;
        this.mCurrentBeat = 0;
        setMetronomeFrequency(sig.getMeter());
    }

    public boolean update(double ticksElapsed) {
        this.mMetronomeProgress += ticksElapsed;
        if (this.mMetronomeProgress < ((double) this.mMetronomeFrequency)) {
            return false;
        }
        this.mMetronomeProgress %= (double) this.mMetronomeFrequency;
        this.mCurrentBeat = (this.mCurrentBeat + 1) % this.mSignature.getNumerator();
        if (this.mCurrentBeat == 0) {
            this.mCurrentMeasure++;
        }
        return true;
    }

    public void setMetronomeFrequency(int meter) {
        switch (meter) {
            case TimeSignature.METER_EIGHTH /*12*/:
                this.mMetronomeFrequency = this.mResolution / 2;
            case TimeSignature.METER_QUARTER /*24*/:
                this.mMetronomeFrequency = this.mResolution;
            case TimeSignature.METER_HALF /*48*/:
                this.mMetronomeFrequency = this.mResolution * 2;
            case TimeSignature.METER_WHOLE /*96*/:
                this.mMetronomeFrequency = this.mResolution * 4;
            default:
        }
    }

    public int getBeatNumber() {
        return this.mCurrentBeat + 1;
    }

    public int getMeasure() {
        return this.mCurrentMeasure;
    }

    public String toString() {
        return "Metronome: " + this.mCurrentMeasure + "\t" + getBeatNumber();
    }

    public int compareTo(MidiEvent o) {
        return 0;
    }

    protected int getEventSize() {
        return 0;
    }

    public int getSize() {
        return 0;
    }
}
