package com.jamesdhong.midi.util;

import com.jamesdhong.midi.event.MidiEvent;

public interface MidiEventListener {
    void onEvent(MidiEvent midiEvent, long j);

    void onStart(boolean z);

    void onStop(boolean z);
}
