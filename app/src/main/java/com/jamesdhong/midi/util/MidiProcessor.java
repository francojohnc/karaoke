package com.jamesdhong.midi.util;

import com.jamesdhong.midi.MidiFile;
import com.jamesdhong.midi.MidiTrack;
import com.jamesdhong.midi.event.MidiEvent;
import com.jamesdhong.midi.event.meta.Tempo;
import com.jamesdhong.midi.event.meta.TimeSignature;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class MidiProcessor {
    private static final int PROCESS_RATE_MS = 8;
    private MidiTrackEventQueue[] mEventQueues;
    private HashMap<Class<? extends MidiEvent>, ArrayList<MidiEventListener>> mEventsToListeners;
    private HashMap<MidiEventListener, ArrayList<Class<? extends MidiEvent>>> mListenersToEvents;
    private int mMPQN;
    private MetronomeTick mMetronome;
    private MidiFile mMidiFile;
    private long mMsElapsed;
    private int mPPQ;
    private boolean mRunning;
    private double mTicksElapsed;

    /* renamed from: com.jamesdhong.midi.util.MidiProcessor.1 */
    class C03751 implements Runnable {
        C03751() {
        }

        public void run() {
            MidiProcessor.this.process();
        }
    }

    private class MidiTrackEventQueue {
        private ArrayList<MidiEvent> mEventsToDispatch;
        private Iterator<MidiEvent> mIterator;
        private MidiEvent mNext;
        private MidiTrack mTrack;

        public MidiTrackEventQueue(MidiTrack track) {
            this.mTrack = track;
            this.mIterator = this.mTrack.getEvents().iterator();
            this.mEventsToDispatch = new ArrayList();
            if (this.mIterator.hasNext()) {
                this.mNext = (MidiEvent) this.mIterator.next();
            }
        }

        public ArrayList<MidiEvent> getNextEventsUpToTick(double tick) {
            this.mEventsToDispatch.clear();
            while (this.mNext != null && ((double) this.mNext.getTick()) <= tick) {
                this.mEventsToDispatch.add(this.mNext);
                if (this.mIterator.hasNext()) {
                    this.mNext = (MidiEvent) this.mIterator.next();
                } else {
                    this.mNext = null;
                }
            }
            return this.mEventsToDispatch;
        }

        public boolean hasMoreEvents() {
            return this.mNext != null;
        }
    }

    public MidiProcessor(MidiFile input) {
        this.mMidiFile = input;
        this.mMPQN = Tempo.DEFAULT_MPQN;
        this.mPPQ = this.mMidiFile.getResolution();
        this.mEventsToListeners = new HashMap();
        this.mListenersToEvents = new HashMap();
        this.mMetronome = new MetronomeTick(new TimeSignature(), this.mPPQ);
        reset();
    }

    public synchronized void start() {
        if (!this.mRunning) {
            this.mRunning = true;
            new Thread(new C03751()).start();
        }
    }

    public void stop() {
        this.mRunning = false;
    }

    public void reset() {
        this.mRunning = false;
        this.mTicksElapsed = 0.0d;
        this.mMsElapsed = 0;
        this.mMetronome.setTimeSignature(new TimeSignature());
        ArrayList<MidiTrack> tracks = this.mMidiFile.getTracks();
        if (this.mEventQueues == null) {
            this.mEventQueues = new MidiTrackEventQueue[tracks.size()];
        }
        for (int i = 0; i < tracks.size(); i++) {
            this.mEventQueues[i] = new MidiTrackEventQueue((MidiTrack) tracks.get(i));
        }
    }

    public boolean isStarted() {
        return this.mTicksElapsed > 0.0d;
    }

    public boolean isRunning() {
        return this.mRunning;
    }

    protected void onStart(boolean fromBeginning) {
        for (MidiEventListener mel : this.mListenersToEvents.keySet()) {
            mel.onStart(fromBeginning);
        }
    }

    protected void onStop(boolean finished) {
        for (MidiEventListener mel : this.mListenersToEvents.keySet()) {
            mel.onStop(finished);
        }
    }

    public void registerEventListener(MidiEventListener mel, Class<? extends MidiEvent> event) {
        ArrayList<MidiEventListener> listeners = (ArrayList) this.mEventsToListeners.get(event);
        if (listeners == null) {
            listeners = new ArrayList();
            listeners.add(mel);
            this.mEventsToListeners.put(event, listeners);
        } else {
            listeners.add(mel);
        }
        ArrayList<Class<? extends MidiEvent>> events = (ArrayList) this.mListenersToEvents.get(mel);
        if (events == null) {
            events = new ArrayList();
            events.add(event);
            this.mListenersToEvents.put(mel, events);
            return;
        }
        events.add(event);
    }

    public void unregisterEventListener(MidiEventListener mel) {
        ArrayList<Class<? extends MidiEvent>> events = (ArrayList) this.mListenersToEvents.get(mel);
        if (events != null) {
            Iterator it = events.iterator();
            while (it.hasNext()) {
                ((ArrayList) this.mEventsToListeners.get((Class) it.next())).remove(mel);
            }
            this.mListenersToEvents.remove(mel);
        }
    }

    public void unregisterEventListener(MidiEventListener mel, Class<? extends MidiEvent> event) {
        ArrayList<MidiEventListener> listeners = (ArrayList) this.mEventsToListeners.get(event);
        if (listeners != null) {
            listeners.remove(mel);
        }
        ArrayList<Class<? extends MidiEvent>> events = (ArrayList) this.mListenersToEvents.get(mel);
        if (events != null) {
            events.remove(event);
        }
    }

    public void unregisterAllEventListeners() {
        this.mEventsToListeners.clear();
        this.mListenersToEvents.clear();
    }

    protected void dispatch(MidiEvent event) {
        boolean shouldDispatch = true;
        if (event.getClass().equals(Tempo.class)) {
            this.mMPQN = ((Tempo) event).getMpqn();
        } else if (event.getClass().equals(TimeSignature.class)) {
            if (this.mMetronome.getBeatNumber() == 1) {
                shouldDispatch = false;
            }
            this.mMetronome.setTimeSignature((TimeSignature) event);
            if (shouldDispatch) {
                dispatch(this.mMetronome);
            }
        }
        sendOnEventForClass(event, event.getClass());
        sendOnEventForClass(event, MidiEvent.class);
    }

    private void sendOnEventForClass(MidiEvent event, Class<? extends MidiEvent> eventClass) {
        ArrayList<MidiEventListener> listeners = (ArrayList) this.mEventsToListeners.get(eventClass);
        if (listeners != null) {
            Iterator it = listeners.iterator();
            while (it.hasNext()) {
                ((MidiEventListener) it.next()).onEvent(event, this.mMsElapsed);
            }
        }
    }

    private void process() {
        onStart(this.mTicksElapsed < 1.0d);
        long lastMs = System.currentTimeMillis();
        boolean finished = false;
        while (this.mRunning) {
            long now = System.currentTimeMillis();
            long msElapsed = now - lastMs;
            if (msElapsed < 8) {
                try {
                    Thread.sleep(8 - msElapsed);
                } catch (Exception e) {
                }
            } else {
                double ticksElapsed = MidiUtil.msToTicks(msElapsed, this.mMPQN, this.mPPQ);
                if (ticksElapsed >= 1.0d) {
                    if (this.mMetronome.update(ticksElapsed)) {
                        dispatch(this.mMetronome);
                    }
                    lastMs = now;
                    this.mMsElapsed += msElapsed;
                    this.mTicksElapsed += ticksElapsed;
                    boolean more = false;
                    int i = 0;
                    while (true) {
                        int length = this.mEventQueues.length;
//                        if (i >= r0) {
//                            break;
//                        }
                        MidiTrackEventQueue queue = this.mEventQueues[i];
                        if (queue.hasMoreEvents()) {
                            Iterator it = queue.getNextEventsUpToTick(this.mTicksElapsed).iterator();
                            while (it.hasNext()) {
                                dispatch((MidiEvent) it.next());
                            }
                            if (queue.hasMoreEvents()) {
                                more = true;
                            }
                        }
                        i++;
                    }
//                    if (!more) {
////                        finished = true;
////                        break;
//                    }
                } else {
                    continue;
                }
            }
        }
        this.mRunning = false;
        onStop(finished);
    }
}
