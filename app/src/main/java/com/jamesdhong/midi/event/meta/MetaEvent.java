package com.jamesdhong.midi.event.meta;

import android.support.v4.view.MotionEventCompat;

import com.jamesdhong.midi.event.MidiEvent;
import com.jamesdhong.midi.util.VariableLengthInt;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public abstract class MetaEvent extends MidiEvent {
    public static final int COPYRIGHT_NOTICE = 2;
    public static final int CUE_POINT = 7;
    public static final int END_OF_TRACK = 47;
    public static final int INSTRUMENT_NAME = 4;
    public static final int KEY_SIGNATURE = 89;
    public static final int LYRICS = 5;
    public static final int MARKER = 6;
    public static final int MIDI_CHANNEL_PREFIX = 32;
    public static final int SEQUENCER_SPECIFIC = 127;
    public static final int SEQUENCE_NUMBER = 0;
    public static final int SMPTE_OFFSET = 84;
    public static final int TEMPO = 81;
    public static final int TEXT_EVENT = 1;
    public static final int TIME_SIGNATURE = 88;
    public static final int TRACK_NAME = 3;
    protected VariableLengthInt mLength;
    protected int mType;

    protected static class MetaEventData {
        public final byte[] data;
        public final VariableLengthInt length;
        public final int type;

        public MetaEventData(InputStream in) throws IOException {
            this.type = in.read();
            this.length = new VariableLengthInt(in);
            this.data = new byte[this.length.getValue()];
            if (this.length.getValue() > 0) {
                in.read(this.data);
            }
        }
    }

    protected abstract int getEventSize();

    protected MetaEvent(long tick, long delta, int type, VariableLengthInt length) {
        super(tick, delta);
        this.mType = type & MotionEventCompat.ACTION_MASK;
        this.mLength = length;
    }

    public void writeToFile(OutputStream out, boolean writeType) throws IOException {
        writeToFile(out);
    }

    protected void writeToFile(OutputStream out) throws IOException {
        super.writeToFile(out, true);
        out.write(MotionEventCompat.ACTION_MASK);
        out.write(this.mType);
    }

    public static MetaEvent parseMetaEvent(long tick, long delta, InputStream in) throws IOException {
        MetaEventData metaEventData = new MetaEventData(in);
        boolean isText = false;
        switch (metaEventData.type) {
            case SEQUENCE_NUMBER /*0*/:
            case MIDI_CHANNEL_PREFIX /*32*/:
            case END_OF_TRACK /*47*/:
            case TEMPO /*81*/:
            case SMPTE_OFFSET /*84*/:
            case TIME_SIGNATURE /*88*/:
            case KEY_SIGNATURE /*89*/:
                break;
            default:
                isText = true;
                break;
        }
        if (isText) {
            String text = new String(metaEventData.data);
            switch (metaEventData.type) {
                case TEXT_EVENT /*1*/:
                    return new Text(tick, delta, text);
                case COPYRIGHT_NOTICE /*2*/:
                    return new CopyrightNotice(tick, delta, text);
                case TRACK_NAME /*3*/:
                    return new TrackName(tick, delta, text);
                case INSTRUMENT_NAME /*4*/:
                    return new InstrumentName(tick, delta, text);
                case LYRICS /*5*/:
                    return new Lyrics(tick, delta, text);
                case MARKER /*6*/:
                    return new Marker(tick, delta, text);
                case CUE_POINT /*7*/:
                    return new CuePoint(tick, delta, text);
                case SEQUENCER_SPECIFIC /*127*/:
                    return new SequencerSpecificEvent(tick, delta, metaEventData.data);
                default:
                    return new GenericMetaEvent(tick, delta, metaEventData);
            }
        }
        switch (metaEventData.type) {
            case SEQUENCE_NUMBER /*0*/:
                return SequenceNumber.parseSequenceNumber(tick, delta, metaEventData);
            case MIDI_CHANNEL_PREFIX /*32*/:
                return MidiChannelPrefix.parseMidiChannelPrefix(tick, delta, metaEventData);
            case END_OF_TRACK /*47*/:
                return new EndOfTrack(tick, delta);
            case TEMPO /*81*/:
                return Tempo.parseTempo(tick, delta, metaEventData);
            case SMPTE_OFFSET /*84*/:
                return SmpteOffset.parseSmpteOffset(tick, delta, metaEventData);
            case TIME_SIGNATURE /*88*/:
                return TimeSignature.parseTimeSignature(tick, delta, metaEventData);
            case KEY_SIGNATURE /*89*/:
                return KeySignature.parseKeySignature(tick, delta, metaEventData);
            default:
                System.out.println("Completely broken in MetaEvent.parseMetaEvent()");
                return null;
        }
    }
}
