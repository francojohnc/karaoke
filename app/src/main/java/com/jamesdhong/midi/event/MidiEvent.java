package com.jamesdhong.midi.event;

import android.support.v4.view.MotionEventCompat;

import com.jamesdhong.midi.event.meta.MetaEvent;
import com.jamesdhong.midi.util.VariableLengthInt;
import com.jamesdhong.videooke.BuildConfig;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public abstract class MidiEvent implements Comparable<MidiEvent> {
    private static int sChannel;
    private static int sId;
    private static int sType;
    protected VariableLengthInt mDelta;
    protected long mTick;

    protected abstract int getEventSize();

    public MidiEvent(long tick, long delta) {
        this.mTick = tick;
        this.mDelta = new VariableLengthInt((int) delta);
    }

    public long getTick() {
        return this.mTick;
    }

    public long getDelta() {
        return (long) this.mDelta.getValue();
    }

    public void setDelta(long d) {
        this.mDelta.setValue((int) d);
    }

    public int getSize() {
        return getEventSize() + this.mDelta.getByteCount();
    }

    public boolean requiresStatusByte(MidiEvent prevEvent) {
        if (prevEvent == null || (this instanceof MetaEvent) || !getClass().equals(prevEvent.getClass())) {
            return true;
        }
        return false;
    }

    public void writeToFile(OutputStream out, boolean writeType) throws IOException {
        out.write(this.mDelta.getBytes());
    }

    static {
        sId = -1;
        sType = -1;
        sChannel = -1;
    }

    public static final MidiEvent parseEvent(long tick, long delta, InputStream in) throws IOException {
        in.mark(1);
        boolean reset = false;
        if (!verifyIdentifier(in.read())) {
            in.reset();
            reset = true;
        }
        if (sType >= 8 && sType <= 14) {
            return ChannelEvent.parseChannelEvent(tick, delta, sType, sChannel, in);
        } else if (sId == MotionEventCompat.ACTION_MASK) {
            return MetaEvent.parseMetaEvent(tick, delta, in);
        } else {
            if (sId == 240 || sId == 247) {
                byte[] data = new byte[new VariableLengthInt(in).getValue()];
                in.read(data);
                return new SystemExclusiveEvent(sId, tick, delta, data);
            }
            System.out.println("Unable to handle status byte, skipping: " + sId);
            if (reset) {
                in.read();
            }
            return null;
        }
    }

    private static boolean verifyIdentifier(int id) {
        sId = id;
        int type = id >> 4;
        int channel = id & 15;
        if (type >= 8 && type <= 14) {
            sId = id;
            sType = type;
            sChannel = channel;
        } else if (id == MotionEventCompat.ACTION_MASK) {
            sId = id;
            sType = -1;
            sChannel = -1;
        } else if (type != 15) {
            return false;
        } else {
            sId = id;
            sType = type;
            sChannel = -1;
        }
        return true;
    }

    public String toString() {
        return BuildConfig.FLAVOR + this.mTick + " (" + this.mDelta.getValue() + "): " + getClass().getSimpleName();
    }
}
