package com.jamesdhong.midi.event;

import android.support.v4.view.MotionEventCompat;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;

public class ChannelEvent extends MidiEvent {
    public static final int CHANNEL_AFTERTOUCH = 13;
    public static final int CONTROLLER = 11;
    public static final int NOTE_AFTERTOUCH = 10;
    public static final int NOTE_OFF = 8;
    public static final int NOTE_ON = 9;
    public static final int PITCH_BEND = 14;
    public static final int PROGRAM_CHANGE = 12;
    private static HashMap<Integer, Integer> mOrderMap;
    protected int mChannel;
    protected int mType;
    protected int mValue1;
    protected int mValue2;

    protected ChannelEvent(long tick, int type, int channel, int param1, int param2) {
        this(tick, 0, type, channel, param1, param2);
    }

    protected ChannelEvent(long tick, long delta, int type, int channel, int param1, int param2) {
        super(tick, delta);
        this.mType = type & 15;
        this.mChannel = channel & 15;
        this.mValue1 = param1 & MotionEventCompat.ACTION_MASK;
        this.mValue2 = param2 & MotionEventCompat.ACTION_MASK;
    }

    public int getType() {
        return this.mType;
    }

    public void setChannel(int c) {
        if (c < 0) {
            c = 0;
        } else if (c > 15) {
            c = 15;
        }
        this.mChannel = c;
    }

    public int getChannel() {
        return this.mChannel;
    }

    protected int getEventSize() {
        switch (this.mType) {
            case PROGRAM_CHANGE /*12*/:
            case CHANNEL_AFTERTOUCH /*13*/:
                return 2;
            default:
                return 3;
        }
    }

    public int compareTo(MidiEvent other) {
        int i = 1;
        if (this.mTick != other.getTick()) {
            int i2;
            if (this.mTick < other.getTick()) {
                i2 = -1;
            } else {
                i2 = 1;
            }
            return i2;
        } else if (this.mDelta.getValue() != other.mDelta.getValue()) {
            if (this.mDelta.getValue() >= other.mDelta.getValue()) {
                i = -1;
            }
            return i;
        } else if (!(other instanceof ChannelEvent)) {
            return 1;
        } else {
            ChannelEvent o = (ChannelEvent) other;
            if (this.mType != o.getType()) {
                if (mOrderMap == null) {
                    buildOrderMap();
                }
                if (((Integer) mOrderMap.get(Integer.valueOf(this.mType))).intValue() >= ((Integer) mOrderMap.get(Integer.valueOf(o.getType()))).intValue()) {
                    return 1;
                }
                return -1;
            } else if (this.mValue1 != o.mValue1) {
                if (this.mValue1 >= o.mValue1) {
                    return 1;
                }
                return -1;
            } else if (this.mValue2 != o.mValue2) {
                if (this.mValue2 >= o.mValue2) {
                    return 1;
                }
                return -1;
            } else if (this.mChannel == o.getChannel()) {
                return 0;
            } else {
                if (this.mChannel >= o.getChannel()) {
                    return 1;
                }
                return -1;
            }
        }
    }

    public boolean requiresStatusByte(MidiEvent prevEvent) {
        if (prevEvent == null || !(prevEvent instanceof ChannelEvent)) {
            return true;
        }
        ChannelEvent ce = (ChannelEvent) prevEvent;
        if (this.mType == ce.getType() && this.mChannel == ce.getChannel()) {
            return false;
        }
        return true;
    }

    public void writeToFile(OutputStream out, boolean writeType) throws IOException {
        super.writeToFile(out, writeType);
        if (writeType) {
            out.write((this.mType << 4) + this.mChannel);
        }
        out.write(this.mValue1);
        if (this.mType != PROGRAM_CHANGE && this.mType != CHANNEL_AFTERTOUCH) {
            out.write(this.mValue2);
        }
    }

    public static ChannelEvent parseChannelEvent(long tick, long delta, int type, int channel, InputStream in) throws IOException {
        int val1 = in.read();
        int val2 = 0;
        if (!(type == PROGRAM_CHANGE || type == CHANNEL_AFTERTOUCH)) {
            val2 = in.read();
        }
        switch (type) {
            case NOTE_OFF /*8*/:
                return new NoteOff(tick, delta, channel, val1, val2);
            case NOTE_ON /*9*/:
                return new NoteOn(tick, delta, channel, val1, val2);
            case NOTE_AFTERTOUCH /*10*/:
                return new NoteAftertouch(tick, delta, channel, val1, val2);
            case CONTROLLER /*11*/:
                return new Controller(tick, delta, channel, val1, val2);
            case PROGRAM_CHANGE /*12*/:
                return new ProgramChange(tick, delta, channel, val1);
            case CHANNEL_AFTERTOUCH /*13*/:
                return new ChannelAftertouch(tick, delta, channel, val1);
            case PITCH_BEND /*14*/:
                return new PitchBend(tick, delta, channel, val1, val2);
            default:
                return new ChannelEvent(tick, delta, type, channel, val1, val2);
        }
    }

    private static void buildOrderMap() {
        mOrderMap = new HashMap();
        mOrderMap.put(Integer.valueOf(PROGRAM_CHANGE), Integer.valueOf(0));
        mOrderMap.put(Integer.valueOf(CONTROLLER), Integer.valueOf(1));
        mOrderMap.put(Integer.valueOf(NOTE_ON), Integer.valueOf(2));
        mOrderMap.put(Integer.valueOf(NOTE_OFF), Integer.valueOf(3));
        mOrderMap.put(Integer.valueOf(NOTE_AFTERTOUCH), Integer.valueOf(4));
        mOrderMap.put(Integer.valueOf(CHANNEL_AFTERTOUCH), Integer.valueOf(5));
        mOrderMap.put(Integer.valueOf(PITCH_BEND), Integer.valueOf(6));
    }
}
