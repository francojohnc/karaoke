package com.jamesdhong.midi.event.meta;

import com.jamesdhong.midi.event.MidiEvent;
import com.jamesdhong.midi.util.VariableLengthInt;

import java.io.IOException;
import java.io.OutputStream;

public class TimeSignature extends MetaEvent {
    public static final int DEFAULT_DIVISION = 8;
    public static final int DEFAULT_METER = 24;
    public static final int METER_EIGHTH = 12;
    public static final int METER_HALF = 48;
    public static final int METER_QUARTER = 24;
    public static final int METER_WHOLE = 96;
    private int mDenominator;
    private int mDivision;
    private int mMeter;
    private int mNumerator;

    public TimeSignature() {
        this(0, 0, 4, 4, METER_QUARTER, DEFAULT_DIVISION);
    }

    public TimeSignature(long tick, long delta, int num, int den, int meter, int div) {
        super(tick, delta, 88, new VariableLengthInt(4));
        setTimeSignature(num, den, meter, div);
    }

    public void setTimeSignature(int num, int den, int meter, int div) {
        this.mNumerator = num;
        this.mDenominator = log2(den);
        this.mMeter = meter;
        this.mDivision = div;
    }

    public int getNumerator() {
        return this.mNumerator;
    }

    public int getDenominatorValue() {
        return this.mDenominator;
    }

    public int getRealDenominator() {
        return (int) Math.pow(2.0d, (double) this.mDenominator);
    }

    public int getMeter() {
        return this.mMeter;
    }

    public int getDivision() {
        return this.mDivision;
    }

    protected int getEventSize() {
        return 7;
    }

    public void writeToFile(OutputStream out) throws IOException {
        super.writeToFile(out);
        out.write(4);
        out.write(this.mNumerator);
        out.write(this.mDenominator);
        out.write(this.mMeter);
        out.write(this.mDivision);
    }

    public static MetaEvent parseTimeSignature(long tick, long delta, MetaEventData info) {
        if (info.length.getValue() != 4) {
            return new GenericMetaEvent(tick, delta, info);
        }
        int num = info.data[0];
        int den = info.data[1];
        return new TimeSignature(tick, delta, num, (int) Math.pow(2.0d, (double) den), info.data[2], info.data[3]);
    }

    private int log2(int den) {
        switch (den) {
            case SmpteOffset.FRAME_RATE_30_DROP /*2*/:
                return 1;
            case MetaEvent.INSTRUMENT_NAME /*4*/:
                return 2;
            case DEFAULT_DIVISION /*8*/:
                return 3;
            case MetaEvent.MIDI_CHANNEL_PREFIX /*32*/:
                return 5;
            default:
                return 0;
        }
    }

    public String toString() {
        return super.toString() + " " + this.mNumerator + "/" + getRealDenominator();
    }

    public int compareTo(MidiEvent other) {
        int i = 1;
        if (this.mTick != other.getTick()) {
            if (this.mTick < other.getTick()) {
                return -1;
            }
            return 1;
        } else if (((long) this.mDelta.getValue()) != other.getDelta()) {
            if (((long) this.mDelta.getValue()) >= other.getDelta()) {
                i = -1;
            }
            return i;
        } else if (!(other instanceof TimeSignature)) {
            return 1;
        } else {
            TimeSignature o = (TimeSignature) other;
            if (this.mNumerator != o.mNumerator) {
                if (this.mNumerator >= o.mNumerator) {
                    return 1;
                }
                return -1;
            } else if (this.mDenominator == o.mDenominator) {
                return 0;
            } else {
                if (this.mDenominator >= o.mDenominator) {
                    return 1;
                }
                return -1;
            }
        }
    }
}
