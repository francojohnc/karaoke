package com.jamesdhong.midi.event.meta;

import com.jamesdhong.midi.event.MidiEvent;
import com.jamesdhong.midi.util.VariableLengthInt;

import java.io.IOException;
import java.io.OutputStream;

public class KeySignature extends MetaEvent {
    public static final int SCALE_MAJOR = 0;
    public static final int SCALE_MINOR = 1;
    private int mKey;
    private int mScale;

    public KeySignature(long tick, long delta, int key, int scale) {
        super(tick, delta, 89, new VariableLengthInt(2));
        setKey(key);
        this.mScale = scale;
    }

    public void setKey(int key) {
        this.mKey = (byte) key;
        if (this.mKey < -7) {
            this.mKey = -7;
        } else if (this.mKey > 7) {
            this.mKey = 7;
        }
    }

    public int getKey() {
        return this.mKey;
    }

    public void setScale(int scale) {
        this.mScale = scale;
    }

    public int getScale() {
        return this.mScale;
    }

    protected int getEventSize() {
        return 5;
    }

    public void writeToFile(OutputStream out) throws IOException {
        super.writeToFile(out);
        out.write(2);
        out.write(this.mKey);
        out.write(this.mScale);
    }

    public static MetaEvent parseKeySignature(long tick, long delta, MetaEventData info) {
        if (info.length.getValue() != 2) {
            return new GenericMetaEvent(tick, delta, info);
        }
        return new KeySignature(tick, delta, info.data[SCALE_MAJOR], info.data[SCALE_MINOR]);
    }

    public int compareTo(MidiEvent other) {
        int i = SCALE_MINOR;
        if (this.mTick != other.getTick()) {
            if (this.mTick < other.getTick()) {
                return -1;
            }
            return SCALE_MINOR;
        } else if (((long) this.mDelta.getValue()) != other.getDelta()) {
            if (((long) this.mDelta.getValue()) >= other.getDelta()) {
                i = -1;
            }
            return i;
        } else if (!(other instanceof KeySignature)) {
            return SCALE_MINOR;
        } else {
            KeySignature o = (KeySignature) other;
            if (this.mKey != o.mKey) {
                if (this.mKey >= o.mKey) {
                    return SCALE_MINOR;
                }
                return -1;
            } else if (this.mScale == o.mScale) {
                return SCALE_MAJOR;
            } else {
                if (this.mKey >= o.mScale) {
                    return SCALE_MINOR;
                }
                return -1;
            }
        }
    }
}
