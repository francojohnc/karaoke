package com.jamesdhong.midi.event.meta;

import android.support.v4.view.MotionEventCompat;

import com.jamesdhong.midi.event.MidiEvent;
import com.jamesdhong.midi.util.VariableLengthInt;

import java.io.IOException;
import java.io.OutputStream;

public class SequenceNumber extends MetaEvent {
    private int mNumber;

    public SequenceNumber(long tick, long delta, int number) {
        super(tick, delta, 0, new VariableLengthInt(2));
        this.mNumber = number;
    }

    public int getMostSignificantBits() {
        return this.mNumber >> 8;
    }

    public int getLeastSignificantBits() {
        return this.mNumber & MotionEventCompat.ACTION_MASK;
    }

    public int getSequenceNumber() {
        return this.mNumber;
    }

    public void writeToFile(OutputStream out) throws IOException {
        super.writeToFile(out);
        out.write(2);
        out.write(getMostSignificantBits());
        out.write(getLeastSignificantBits());
    }

    public static MetaEvent parseSequenceNumber(long tick, long delta, MetaEventData info) {
        if (info.length.getValue() != 2) {
            return new GenericMetaEvent(tick, delta, info);
        }
        int msb = info.data[0];
        return new SequenceNumber(tick, delta, (msb << 8) + info.data[1]);
    }

    protected int getEventSize() {
        return 5;
    }

    public int compareTo(MidiEvent other) {
        int i = 1;
        if (this.mTick != other.getTick()) {
            if (this.mTick < other.getTick()) {
                return -1;
            }
            return 1;
        } else if (((long) this.mDelta.getValue()) != other.getDelta()) {
            if (((long) this.mDelta.getValue()) >= other.getDelta()) {
                i = -1;
            }
            return i;
        } else if (!(other instanceof SequenceNumber)) {
            return 1;
        } else {
            SequenceNumber o = (SequenceNumber) other;
            if (this.mNumber == o.mNumber) {
                return 0;
            }
            if (this.mNumber >= o.mNumber) {
                return 1;
            }
            return -1;
        }
    }
}
