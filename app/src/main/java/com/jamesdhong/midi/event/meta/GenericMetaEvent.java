package com.jamesdhong.midi.event.meta;

import com.jamesdhong.midi.event.MidiEvent;

import java.io.IOException;
import java.io.OutputStream;

public class GenericMetaEvent extends MetaEvent {
    private byte[] mData;

    protected GenericMetaEvent(long tick, long delta, MetaEventData info) {
        super(tick, delta, info.type, info.length);
        this.mData = info.data;
        System.out.println("Warning: GenericMetaEvent used because type (" + info.type + ") wasn't recognized or unexpected data length (" + info.length.getValue() + ") for type.");
    }

    protected int getEventSize() {
        return (this.mLength.getByteCount() + 2) + this.mLength.getValue();
    }

    protected void writeToFile(OutputStream out) throws IOException {
        super.writeToFile(out);
        out.write(this.mLength.getBytes());
        out.write(this.mData);
    }

    public int compareTo(MidiEvent other) {
        int i = -1;
        if (this.mTick != other.getTick()) {
            if (this.mTick >= other.getTick()) {
                i = 1;
            }
            return i;
        } else if (((long) this.mDelta.getValue()) == other.getDelta() || ((long) this.mDelta.getValue()) < other.getDelta()) {
            return 1;
        } else {
            return -1;
        }
    }
}
