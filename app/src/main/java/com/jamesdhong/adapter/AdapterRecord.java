package com.jamesdhong.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jamesdhong.videooke.R;

import java.io.File;
import java.util.ArrayList;


public class AdapterRecord extends RecyclerView.Adapter<AdapterRecord.ViewHolder>{
    private ArrayList<File> fileList;
    public AdapterRecord(ArrayList<File> fileList) {
        this.fileList = fileList;
    }
    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = (LayoutInflater) viewGroup.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final ViewHolder v = new ViewHolder(inflater.inflate(R.layout.item_record,viewGroup, false));
        v.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                File recordFile = fileList.get(v.getAdapterPosition());
                Intent i = new Intent();
                i.setAction(android.content.Intent.ACTION_VIEW);
                File file = new File(recordFile.getPath());
                i.setDataAndType(Uri.fromFile(file), "audio/*");
                v.itemView.getContext().startActivity(i);
//                Intent i = new Intent(v.itemView.getContext(), VideokePlayer.class);
//                File kar = fileList.get(v.getAdapterPosition());
//                i.putExtra(Constant.KAR_FILE_KEY,kar);
//                v.itemView.getContext().startActivity(i);
            }
        });
        return v;
    }
    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
        viewHolder.txt_title.setText(fileList.get(position).getName());
    }
    @Override
    public int getItemCount() {
        return fileList.size();
    }
    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_title;
        public ViewHolder(View v) {
            super(v);
            txt_title = (TextView) v.findViewById(R.id.txt_title);
        }
    }
}