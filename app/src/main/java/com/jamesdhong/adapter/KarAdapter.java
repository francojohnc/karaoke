package com.jamesdhong.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.jamesdhong.Constant;
import com.jamesdhong.VideokePlayer;
import com.jamesdhong.videooke.R;

import java.io.File;
import java.util.ArrayList;


public class KarAdapter  extends RecyclerView.Adapter<KarAdapter.ViewHolder>{
    private ArrayList<File> fileList;
    public KarAdapter(ArrayList<File> fileList) {
        this.fileList = fileList;
    }
    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = (LayoutInflater) viewGroup.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final ViewHolder v = new ViewHolder(inflater.inflate(R.layout.item_kar,viewGroup, false));
        v.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(v.itemView.getContext(), VideokePlayer.class);
                File kar = fileList.get(v.getAdapterPosition());
                i.putExtra(Constant.KAR_FILE_KEY,kar);
                v.itemView.getContext().startActivity(i);
            }
        });
        return v;
    }
    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
        viewHolder.txt_title.setText(fileList.get(position).getName().replace(".kar",""));
        viewHolder.btn_record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(viewHolder.itemView.getContext(), VideokePlayer.class);
                File kar = fileList.get(position);
                i.putExtra(Constant.KAR_FILE_KEY,kar);
                i.putExtra(Constant.KAR_RECORD_KEY,true);
                viewHolder.itemView.getContext().startActivity(i);
            }
        });
    }
    @Override
    public int getItemCount() {
        return fileList.size();
    }
    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_title;
        Button btn_record;
        public ViewHolder(View v) {
            super(v);
            txt_title = (TextView) v.findViewById(R.id.txt_title);
            btn_record = (Button) v.findViewById(R.id.btn_record);
        }
    }
}