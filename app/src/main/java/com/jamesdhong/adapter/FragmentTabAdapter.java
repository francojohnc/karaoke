/**
 * Created by john carlo franco on 09/05/2015.
 * johncarlofranco.com
 */
package com.jamesdhong.adapter;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import java.util.LinkedHashMap;

public class FragmentTabAdapter extends FragmentPagerAdapter {
	private LinkedHashMap<Fragment,String> mapFragment = new LinkedHashMap<>();
	private LinkedHashMap<Fragment,Integer> mapFragmenIcon = new LinkedHashMap<>();
	private TabLayout tabLayout;
	public FragmentTabAdapter(FragmentManager fragmentManager, LinkedHashMap<Fragment, String> mapFragment) {
		super(fragmentManager);
		this.mapFragment= mapFragment;
	}
	public FragmentTabAdapter(FragmentManager fragmentManager) {
		super(fragmentManager);
	}
	public void addFragment(Fragment fragment,String title){
		mapFragment.put(fragment, title);
	}
	public void addFragment(Fragment fragment,int icon){
		mapFragmenIcon.put(fragment, icon);
	}
	public void addTabLayout(TabLayout tabLayout,ViewPager viewPager){
		tabLayout.setupWithViewPager(viewPager);
		this.tabLayout=tabLayout;
		for(int i=0;i<mapFragmenIcon.size();i++){
			tabLayout.getTabAt(i).setIcon((Integer) mapFragmenIcon.values().toArray()[i]);
		}
	}
	@Override
	public CharSequence getPageTitle (int position){
		return mapFragment.size()>0 ? (CharSequence) mapFragment.values().toArray()[position] : null;
	}
	@Override
	public Fragment getItem(int position) {
		return mapFragment.size()>0? (Fragment) mapFragment.keySet().toArray()[position] : (Fragment) mapFragmenIcon.keySet().toArray()[position];
	}
	@Override
	public int getCount() {
		return mapFragment.size()>0? mapFragment.size():mapFragmenIcon.size();
	}
}
