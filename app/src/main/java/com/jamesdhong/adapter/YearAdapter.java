package com.jamesdhong.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.jamesdhong.Constant;
import com.jamesdhong.GenreActivity;
import com.jamesdhong.videooke.R;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by FRANCO on 8/6/2016.
 */
public class YearAdapter  extends RecyclerView.Adapter<YearAdapter.ViewHolder>{
    private HashMap<String,HashMap<String,ArrayList<File>>>fileList;
    public YearAdapter(HashMap<String,HashMap<String,ArrayList<File>>>stringArrayListHashMap) {
        this.fileList = stringArrayListHashMap;
    }
    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = (LayoutInflater) viewGroup.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final ViewHolder v = new ViewHolder(inflater.inflate(R.layout.item_year,viewGroup, false));
        v.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(v.itemView.getContext(), GenreActivity.class);
                String key= (String) fileList.keySet().toArray()[v.getAdapterPosition()];
                HashMap<String, ArrayList<File>> files = fileList.get(key);
                i.putExtra(Constant.KAR_FILE_MAP_KEY,files);
                i.putExtra(Constant.KAR_FILE_TITLE_KEY,key);
                v.itemView.getContext().startActivity(i);
            }
        });
        return v;
    }
    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
        String key = (String) fileList.keySet().toArray()[position];
        viewHolder.txt_title.setText(key);
    }
    @Override
    public int getItemCount() {
        return fileList.size();
    }
    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_title;
        Button btn_record;
        public ViewHolder(View v) {
            super(v);
            txt_title = (TextView) v.findViewById(R.id.txt_title);
        }
    }
}