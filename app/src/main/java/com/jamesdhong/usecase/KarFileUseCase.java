package com.jamesdhong.usecase;

import com.jamesdhong.helper.UtilFile;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import rx.Observable;
import rx.Subscriber;

public class KarFileUseCase {
    public static final String TAG =KarFileUseCase.class.getSimpleName();
//        public static final HashMap<String,ArrayList<File>> folderListMap = new HashMap<>();
    public static final HashMap<String,HashMap<String,ArrayList<File>>> folderListYearMap = new HashMap<>();

    public KarFileUseCase(){

    }
    public Observable<HashMap<String,HashMap<String,ArrayList<File>>>> getList (){
        Observable<HashMap<String,HashMap<String,ArrayList<File>>> > fileListObservable = Observable.create(new Observable.OnSubscribe<HashMap<String,HashMap<String,ArrayList<File>>>>(){
            @Override
            public void call(Subscriber<? super HashMap<String,HashMap<String,ArrayList<File>>>> subscriber) {
                if(folderListYearMap.size()==0){
                    getFile(UtilFile.sdCardRootFile());
                }
                subscriber.onNext(folderListYearMap);
                subscriber.onCompleted();
            }
        });
        return fileListObservable;
    }

    private void getFile(File dir) {
        File listFile[] = UtilFile.getListFileFromFolder(dir);
        for (int i = 0; i < listFile.length; i++) {
            if (listFile[i].isDirectory()) {
                getFile(listFile[i]);
            } else {
                boolean validFile = UtilFile.isFileExtension(listFile[i],".kar");
                if (validFile){
                    File yearFolderFile = listFile[i].getParentFile();
                    String yearfolder = yearFolderFile.getParentFile().getName();
                    if(folderListYearMap.containsKey(yearfolder)){
                        HashMap<String,ArrayList<File>> folderListMap =  folderListYearMap.get(yearfolder);
                        /*get second folder*/
                        String folder = listFile[i].getParentFile().getName();
                        if(folderListMap.containsKey(folder)){
                            folderListMap.get(folder).add(listFile[i]);
                        }else{
                            ArrayList<File> listFiles = new ArrayList<>();
                            listFiles.add(listFile[i]);
                            folderListMap.put(folder,listFiles);
                        }
                        folderListYearMap.put(yearfolder,folderListMap);
                    }else{
                        HashMap<String,ArrayList<File>> folderListMap =  new HashMap<>();
                         /*get second folder*/
                        String folder = listFile[i].getParentFile().getName();
                        if(folderListMap.containsKey(folder)){
                            folderListMap.get(folder).add(listFile[i]);
                        }else{
                            ArrayList<File> listFiles = new ArrayList<>();
                            listFiles.add(listFile[i]);
                            folderListMap.put(folder,listFiles);
                        }
                        folderListYearMap.put(yearfolder,folderListMap);

                    }
//                    if(folderListYearMap.containsKey(yearfolder)){
//                        HashMap<String,ArrayList<File>> folderListMap =  folderListYearMap.get(yearfolder);
//                        String folder = listFile[i].getParentFile().getName();
//                        if(folderListMap.containsKey(folder)){
//                            folderListMap.get(folder).add(listFile[i]);
//                        }else{
//                            ArrayList<File> listFiles= new ArrayList<>();
//                            listFiles.add(listFile[i]);
//                            folderListMap.put(folder,listFiles);
//                        }
//                        folderListYearMap.put(yearfolder,folderListMap);
//                    }
//                    String folder = listFile[i].getParentFile().getName();
//                    if(folderListMap.containsKey(folder)){
//                        folderListMap.get(folder).add(listFile[i]);
//                    }else{
//                        ArrayList<File> listFiles= new ArrayList<>();
//                        listFiles.add(listFile[i]);
//                        folderListMap.put(folder,listFiles);
//                    }
                }
            }
        }
    }
}
