package com.jamesdhong;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.Spannable.Factory;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.jamesdhong.videooke.BuildConfig;
import com.jamesdhong.videooke.R;

import java.util.ArrayList;

public class SongLineAdapter extends ArrayAdapter<SongLine> {
    Context context;
    ArrayList<SongLine> songLines;

    public SongLineAdapter(Context context, int resource, ArrayList<SongLine> songLines) {
        super(context, resource, songLines);
        this.context = context;
        this.songLines = songLines;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = ((LayoutInflater) getContext().getSystemService("layout_inflater")).inflate(R.layout.item_song_lines, null);
        }
        TextView tvItemSongLine = (TextView) convertView.findViewById(R.id.tvItemSongLine);
        SongLine songLine = (SongLine) this.songLines.get(position);
        tvItemSongLine.setTextSize(2, songLine.FontSize);
        Spannable spanned = Factory.getInstance().newSpannable(songLine.SongLine.replaceAll("[\n\r]", BuildConfig.FLAVOR));
        if (songLine.isTitle) {
            spanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this.context, R.color.colorPrimary)), 0, spanned.length(), 0);
        } else {
            int endHigh = songLine.SongLineHighlight.length();
            if (endHigh > 0) {
                if (endHigh > spanned.length()) {
                    endHigh = songLine.SongLine.replaceAll("[\n\r]", BuildConfig.FLAVOR).length();
                }
                spanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this.context, R.color.colorPrimary)), 0, endHigh, 0);
            }
        }
        tvItemSongLine.setText(spanned);
        return convertView;
    }
}
