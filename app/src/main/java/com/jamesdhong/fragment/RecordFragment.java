package com.jamesdhong.fragment;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jamesdhong.adapter.AdapterRecord;
import com.jamesdhong.base.BaseFragment;
import com.jamesdhong.helper.UtilFile;
import com.jamesdhong.videooke.R;

import java.io.File;
import java.util.ArrayList;


public class RecordFragment extends BaseFragment{
    private RecyclerView rv_kar;
    private RecyclerView.LayoutManager layoutManager;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_title, container, false);
        instantiate(v);
        String targetFolder = UtilFile.sdCardRoot()+"record"+File.separator;
        File fileFolder = new File(targetFolder);
        getFile(fileFolder);
        setData(fileList);
        return v;
    }
    private ArrayList<File> fileList = new ArrayList<>();
    private void getFile(File dir) {
        File listFile[] = UtilFile.getListFileFromFolder(dir);
        for (int i = 0; i < listFile.length; i++) {
            if (listFile[i].isDirectory()) {
                getFile(listFile[i]);
            } else {
                    fileList.add(listFile[i]);
            }
        }
    }
    private void instantiate(View v) {
        rv_kar = (RecyclerView)v.findViewById(R.id.rv_kar);
        layoutManager = new LinearLayoutManager(getActivity());
    }
    private void setData(ArrayList<File> fileList) {
        //set data
        AdapterRecord adapter = new AdapterRecord(fileList);
        rv_kar.setAdapter(adapter);
        rv_kar.setLayoutManager(layoutManager);
    }
}
