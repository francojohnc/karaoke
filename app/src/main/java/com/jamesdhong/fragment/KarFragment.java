package com.jamesdhong.fragment;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jamesdhong.Constant;
import com.jamesdhong.adapter.KarAdapter;
import com.jamesdhong.base.BaseFragment;
import com.jamesdhong.videooke.R;

import java.io.File;
import java.util.ArrayList;


public class KarFragment extends BaseFragment{
    private RecyclerView rv_kar;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<File> fileList;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_title, container, false);
        instantiate(v);

        ArrayList<File> files= (ArrayList<File>) getArguments().get(Constant.BUNDLE_FRAGMENT_KEY);
        setData(files);
        return v;
    }

    private void instantiate(View v) {
        rv_kar = (RecyclerView)v.findViewById(R.id.rv_kar);
        layoutManager = new LinearLayoutManager(getActivity());
    }
    private void setData(ArrayList<File> fileList) {
        //set data
        KarAdapter adapter = new KarAdapter(fileList);
        rv_kar.setAdapter(adapter);
        rv_kar.setLayoutManager(layoutManager);
    }
}
