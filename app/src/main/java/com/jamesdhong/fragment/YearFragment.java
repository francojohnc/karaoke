package com.jamesdhong.fragment;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jamesdhong.Constant;
import com.jamesdhong.adapter.YearAdapter;
import com.jamesdhong.base.BaseFragment;
import com.jamesdhong.videooke.R;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by FRANCO on 8/6/2016.
 */
public class YearFragment  extends BaseFragment {
    private RecyclerView rv_kar;
    private RecyclerView.LayoutManager layoutManager;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_title, container, false);
        instantiate(v);
        HashMap<String,HashMap<String,ArrayList<File>>> files= (HashMap<String,HashMap<String,ArrayList<File>>>) getArguments().get(Constant.BUNDLE_FRAGMENT_KEY);
        setData(files);
        return v;
    }
    private void instantiate(View v) {
        rv_kar = (RecyclerView)v.findViewById(R.id.rv_kar);
        layoutManager = new LinearLayoutManager(getActivity());
    }
    private void setData(HashMap<String,HashMap<String,ArrayList<File>>>stringArrayListHashMap) {
        //set data
        YearAdapter adapter = new YearAdapter(stringArrayListHashMap);
        rv_kar.setAdapter(adapter);
        rv_kar.setLayoutManager(layoutManager);
    }
}
