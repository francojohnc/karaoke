package com.jamesdhong.playheadset;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.jamesdhong.videooke.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class PlayHeadSetActivity extends AppCompatActivity{
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_headset);
        ButterKnife.bind(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
     finish();
    }

    @OnClick({R.id.btn_ok})
    void onclick(View v) {
        switch (v.getId()){
            case R.id.btn_ok:
                finish();

                break;
        }
    }
}
