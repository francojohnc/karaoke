package com.jamesdhong.helper;

import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

public class ToolbarCreator {
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private Toolbar toolbar;
    private AppCompatActivity appCompatActivity;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    //setup toolbar
    public ToolbarCreator(AppCompatActivity appCompatActivity, int toolbarId){
        this.appCompatActivity=appCompatActivity;
        toolbar = (Toolbar)appCompatActivity.findViewById(toolbarId);
        appCompatActivity.setSupportActionBar(toolbar);//enable menu
    }
    //setters
    //add slide menu
    public void setDrawerLayout(int drawerLayoutId,int navigationViewId){
        drawerLayout = (DrawerLayout)appCompatActivity.findViewById(drawerLayoutId);
        actionBarDrawerToggle = new ActionBarDrawerToggle(appCompatActivity,drawerLayout, toolbar, 0, 0);
        actionBarDrawerToggle.setDrawerIndicatorEnabled(true);
        drawerLayout.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        //add NavigationView
        navigationView = (NavigationView)appCompatActivity.findViewById(navigationViewId);
    }
    public void setTitle(String title){
        appCompatActivity.getSupportActionBar().setTitle(title);
    }
    public void setNavigationIcon(int icon){
        toolbar.setNavigationIcon(icon);
    }
    public void setNavigationItemSelectedListener(NavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener){
        navigationView.setNavigationItemSelectedListener(navigationItemSelectedListener);
    }
    //getters
    public ActionBarDrawerToggle getActionBarDrawerToggle() {
        return actionBarDrawerToggle;
    }
    public Toolbar getToolbar() {
        return toolbar;
    }
    //function
    public void enableBackMenu(){
        appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);//back button
    }
    public void hideTitle(){
        appCompatActivity.getSupportActionBar().setDisplayShowTitleEnabled(false);
    }
    public void closeDrawer(){
        drawerLayout.closeDrawer(GravityCompat.START);
    }
}
