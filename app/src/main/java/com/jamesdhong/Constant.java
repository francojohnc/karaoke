package com.jamesdhong;


public class Constant {
    public static String KAR_FILE_KEY="kar_file";
    public static String KAR_FILE_MAP_KEY="kar_file_map";
    public static String KAR_FILE_TITLE_KEY="kar_file_title";
    public static String KAR_RECORD_KEY="kar_record";
    public static String BUNDLE_FRAGMENT_KEY="kar_record";
}
